FROM node:12.18.0-stretch as node-serve
LABEL authors="Andres Felipe Orozco Morales"
WORKDIR /bikeStoreApp
COPY ./dist .
COPY ./prod .
RUN npm install --production --silent && mv node_modules ../
EXPOSE 9000
CMD ["node", "server.js"]