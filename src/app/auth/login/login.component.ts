import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ICredentials, Credentials } from '../auth-shared/models/credentials';
import { LoginService } from './login.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm = this.fb.group({
    username: ['',
    [Validators.required,
    Validators.minLength(3),
  ]],
    password: ['',[
    Validators.required,
    Validators.minLength(4),
  ]],
  rememberMe: [false]
  });


  constructor(
    private fb: FormBuilder,
    private loginService: LoginService
  ) { }

  ngOnInit(): void {
  }

  login(): void {
    const credentials: ICredentials = new Credentials();

    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;

    this.loginService.login(credentials)
    .subscribe((res: any) => {
      Swal.fire('Ha ingresado correctamente', 'Por favor espere.', 'success');
    }, (error: any) => {
      if (error.status === 401) {
        Swal.fire('Usuario o contrasena', 'incorrectos', 'error');
      }
    });
  }
}
