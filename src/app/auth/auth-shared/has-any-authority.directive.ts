import { Directive, OnDestroy, TemplateRef, ViewContainerRef, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { AccountService } from '../account.service';

@Directive({
  selector: '[appHasAnyAuthority]'
})
export class HasAnyAuthorityDirective implements OnDestroy {
  private authorities: string[] = [];
  private authenticationSubscription?: Subscription

  constructor(
    private accountService: AccountService,
    private templeateRef: TemplateRef<any>,
    private viewContainerRef: ViewContainerRef
  ) { }

    @Input()
    set appHasAnyAuthority(authorities: string[] | string) {
      this.authorities = typeof authorities === 'string' ? [authorities] : authorities;

      this.updateView();
      this.authenticationSubscription = this.accountService.getAuthenticationState().subscribe(() => this.updateView());
    }

      ngOnDestroy(): void{
        if (this.authenticationSubscription) {
          this.authenticationSubscription.unsubscribe();
        }
      }

    private updateView(): void {
      const HasAnyAuthority = this.accountService.hasAnyAuthority(this.authorities)
      this.viewContainerRef.clear();
      if (HasAnyAuthority) {
        this.viewContainerRef.createEmbeddedView(this.templeateRef)
      }
    }
}
