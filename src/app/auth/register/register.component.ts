import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IAccount, Account } from '../auth-shared/models/account.model.ts';
import Swal from 'sweetalert2';
import { LoginService } from '../login/login.service';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})

export class RegisterComponent implements OnInit {
  user: IAccount;
  saved = false;

  registerForm = this.fb.group({
    login: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(12)
    ]],
    firstName: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]],
    lastName: ['', [
      Validators.required,
      Validators.minLength(4),
      Validators.maxLength(20)
    ]],
    email: ['', [
      Validators.required,
      Validators.email
    ]],
    activated: [false],
    authorities: ['']
  });
  
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private accountService: AccountService
  ) {  }

  ngOnInit(): void {
  }

  registerUser():void {
    this.user = this.registerForm.value;
    console.warn('DATOS DEL USUARIO REGISTRADO', this.user);
    this.saved = true;
    this.accountService.createUser(this.user)
      .subscribe((response) => {
        this.saved = true;
        return response;
      } ,(error) => {
        console.log(error);
    });
  } 

}
