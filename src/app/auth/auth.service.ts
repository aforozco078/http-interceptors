import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ICredentials } from './auth-shared/models/credentials';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient) { }
 
  public login(credentials: ICredentials): Observable<any>{
    const data: ICredentials = {
      username: credentials.username,
      password: credentials.password,
      rememberMe: credentials.rememberMe
    };
    return this.http.post<any>(`${environment.END_POINT}/api/authenticate`, data, {observe: 'response'})
    .pipe(map(res => {
      const bearerToken = res.headers.get ('Authorization');
      if(bearerToken && bearerToken.slice(0, 7) === 'Bearer ') {
        const jwt = bearerToken.slice(7, bearerToken.length);
        this.storeAuthenticationToken(jwt, credentials.rememberMe);
        return jwt;
      }
    }));
  }
      /**Este es un metodo para cerrar la sesion*/
  public logout(): Observable<any> {
    return new Observable(observe => {
      localStorage.removeItem('token');
      sessionStorage.removeItem('token');
      observe.complete();
    });
  }

  private storeAuthenticationToken(jwt: string, rememberMe: boolean): void {
    if(rememberMe) {
      localStorage.setItem('token', jwt);
    } else {
      sessionStorage.setItem('token', jwt);
    }
  }
}