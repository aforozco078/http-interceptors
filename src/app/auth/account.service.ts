import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account, IAccount } from './auth-shared/models/account.model.ts';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { catchError, tap, shareReplay, map } from 'rxjs/operators';
import { ICredentials } from './auth-shared/models/credentials';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

    create(credentials: ICredentials): Observable<ICredentials> {
      return this.http.post<ICredentials>(`${environment.END_POINT}/api/account`, credentials);
    }
    
    updateUser(user: IAccount): Observable<IAccount> {
      return  this.http.put<IAccount>(`${environment.END_POINT}/api/users`, user )
    }
  
    createUser(user: IAccount): Observable<IAccount> {
      return  this.http.post<IAccount>(`${environment.END_POINT}/api/users`, user )
        .pipe(map((res) => {
          return res;
        }, error => {
          return error;
        }));
    }
    
  idendity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache || force || !this.isAuthenticated()) {
      this.accountCache = this.fetch().pipe(catchError(() => {
        return of(null);
      }), tap((account: Account | null) =>{
        this.authenticate(account);
        if(account) {
          this.router.navigate(['/dashboard']);
        }
      }), shareReplay());
    }
    return this.accountCache;
  }

  authenticate(account: Account | null): void{
    this.userIdentity = account;
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }



  //Obtain Datas User
  getUsername(): string{
    return this.userIdentity.login;
  }

  getAuthenticationState(): Observable<Account | null> {
    return  this.authenticationState.asObservable();
  }

  //Verify Rols
  hasAnyAuthority(authorities: string[] | string): boolean {
    if(!this.userIdentity || !this.userIdentity.authorities){
      return false;
    }

    if(!Array.isArray(authorities)){
      authorities = [authorities];
    }

    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
  }


  private fetch(): Observable <Account> {
    return this.http.get<Account>(`${environment.END_POINT}/api/account`);
  }
}
