import { Component, OnInit } from '@angular/core';
import { AccountService } from '../account.service';
import { Account, IAccount } from '../auth-shared/models/account.model.ts';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass']
})
export class ProfileComponent implements OnInit {
  userData: Account | null;
  editForm = this.fb.group({
    id: [''],
    login: [''],
    firstName: [''],
    lastName: [''],
    email: [''],
    activated: [''],
    authorities: ['']
  });
  
  constructor(
    private accountService: AccountService,
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.accountService.idendity()
    .subscribe((res: any) => {
      this.userData = res;
      console.log('USER DATA',this.userData);
    });
    this.updateForm(this.userData);
    
  }

  updateForm(account: Account | null): void {
    this.editForm.patchValue({
      id: account.id,
      login: account.login,
      firstName: account.firstName,
      lastName: account.lastName,
      email: account.email,
      authorities: account.authorities,
      activated: account.activated
    })
  }

  updateAccount(): IAccount {
    return {
      ...new Account(),
      id: this.editForm.get('id').value,
      login: this.editForm.get('login').value,
      firstName: this.editForm.get('firstName').value,
      lastName: this.editForm.get('lastName').value,
      email: this.editForm.get('email').value,
      authorities: this.editForm.get('authorities').value,
      activated: this.editForm.get('activated').value
    };
  }

  /*printAccount(): void {
    const cuenta: IAccount = this.updateAccount();
    console.log(cuenta);
  }*/

  updateData(): void {
    this.accountService.updateUser(this.updateAccount())
    .subscribe(res => {
      Swal.fire('User updated successfully', '', 'success');
      this.router.navigate(['/dashboard']);
    }, error => error);
  }
}


