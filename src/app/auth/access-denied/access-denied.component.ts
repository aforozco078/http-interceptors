import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-access-denied',
  templateUrl: './access-denied.component.html',
  styleUrls: ['./access-denied.component.sass'],

})
export class AccessDeniedComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
  }

}
