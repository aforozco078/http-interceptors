import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { IBikeWithClient } from 'src/app/shared/models/bike-with-client';

@Component({
  selector: 'app-bikes-create',
  templateUrl: './bikes-create.component.html',
  styleUrls: ['./bikes-create.component.sass']
})
export class BikesCreateComponent implements OnInit {

  selectedApprentice: number;

  bikeFormGroup = this.formBuilder.group({
    model: ['', [
                  Validators.required,
                  Validators.minLength(2),
                  Validators.maxLength(5)
                ]],
    price: ['', Validators.required],
    serial: [''],
    status: true
  });

  bikeWithClient: IBikeWithClient;

  constructor(private formBuilder: FormBuilder, private bikeService: BikesService) {}

  ngOnInit() {
  }

  public saveBike(): void {
/*
    if(this.selectedApprentice == undefined){
      console.log('DEBES BUSCAR UN APRENDIZ');
      return;
    } else {
      */
/*
      this.bikeFormGroup.patchValue({
        idUser: this.selectedApprentice
      });*/

      console.log('Data Save', this.bikeFormGroup.value);

      this.bikeWithClient = {
        model: this.bikeFormGroup.value.model,
        price: this.bikeFormGroup.value.price,
        serial: this.bikeFormGroup.value.serial,
        status: true,

        name: 'JHON BRAVO',
        documentNumber: '11223344'
      };

      this.bikeService.saveBike(this.bikeWithClient)
      .subscribe(res => {
        console.log('Save Ok', res);
      }, error => {
        console.log('error al guardar ', error);
      });
   // }
  }

  searchApprentice(): void {
    /*this.userService.search(document).
    subscribe(res =>{ */
      this.selectedApprentice = 5; //res.idUser;
   // })
   
  }

  changeStatus(event: any): void {
    console.warn('VIEW DATA EVENT ',event);

  }

}
