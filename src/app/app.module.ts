import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './auth/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthInterceptor } from './auth/guards/auth.guard';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { RegisterComponent } from './auth/register/register.component';
import { ProfileComponent } from './auth/profile/profile.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AccessDeniedComponent,
    RegisterComponent,
    ProfileComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, /**En este punto, nuestro interceptor modificará cada petición que se haga al servidor siempre y cuando exista un token. */
      useClass: AuthInterceptor,
      multi: true } /**Esto permitira agregar mas interceptors si lo requerimos y no sobre escribir nuestro interceptors, en pocas palabras se crea un array con el mismo token */
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
