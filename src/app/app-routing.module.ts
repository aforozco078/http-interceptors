import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';

import { Authority } from './auth/auth-shared/constants/authority.constants';
import { UserRouteAccessService } from './auth/guards/user-route-access.service';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';


const routes: Routes = [
  {
    path: 'dashboard',
    data: {
      authorities: [
        Authority.ADMIN, 
        Authority.USER]
    },

    canActivate: [UserRouteAccessService],
    loadChildren: () => import('./dashboard/dashboard.module')
    .then(m => m.DashboardModule)
  },

  {
    path: 'auth/login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },

  {
    path: 'accessDenied',
    component: AccessDeniedComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
