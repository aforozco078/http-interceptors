import { Component, OnInit } from '@angular/core';
import { AccountService } from 'src/app/auth/account.service';
import { LoginService } from 'src/app/auth/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  user: string;

  constructor( 
    private accountService: AccountService,
    private loginService: LoginService,
    private router: Router
   ) { }

  ngOnInit(): void {
    this.getName();
  }

  getName(): void{
    this.user = this.accountService.getUsername();
  }

  logout(): void{
    this.loginService.logout();
    this.router.navigate(['auth/login']);
  }

}
