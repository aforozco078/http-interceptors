import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Authority } from '../auth/auth-shared/constants/authority.constants';
import { UserRouteAccessService } from '../auth/guards/user-route-access.service';
import { AccessDeniedComponent } from '../auth/access-denied/access-denied.component';
import { RegisterComponent } from '../auth/register/register.component';
import { ProfileComponent } from '../auth/profile/profile.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'bikes',
        data: {
          authorities: [
            Authority.ADMIN]
        },
      canActivate: [UserRouteAccessService],
        loadChildren: () => import('../components/bikes/bikes.module')
        .then(m => m.BikesModule),
      },

      {
        path: 'sales',
        data: {
          authorities: [
            Authority.USER]
        },
        
      canActivate: [UserRouteAccessService],
        loadChildren: () => import('../components/sales/sales.module')
        .then(m => m.SalesModule)
      },
      
      {
        path: 'accessDenied',
        component: AccessDeniedComponent
      },
       {
         path: 'profile',
         component: ProfileComponent
       },
       {
         path: 'register',
         component: RegisterComponent
       }


    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
